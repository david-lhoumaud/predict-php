# PHP-ML

## Install dépendances

```
composer install
```

## Display help
```
php predict.php -h
```

```
Mep Predict v0.0.1
David Lhoumaud
Options:
    -r    Lance une prédiction
        --action=(files|themes|update|upgrade|install)
        --date='Y-m-d'
    -a    Ajoute une entrée dans la base de donnée json
        --action=(files|themes|update|upgrade|install)
        --date='Y-m-d'
        --result=(done|warning|failed)
    -f    Chemin de la base de donnée json (default: db.json)
    -h    Afficher l'aide
```

## Predict examples

### Run predict files
```
php predict.php -r --action='files' --date='2023-05-16'
```

### Add result in DB predict
```
php predict.php -a --action='files' --date='2023-05-16' --result=done
```
