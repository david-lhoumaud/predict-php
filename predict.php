<?php

require_once __DIR__ . '/vendor/autoload.php';

use Phpml\Classification\DecisionTree;

use Phpml\Regression\SVR;
use Phpml\SupportVectorMachine\Kernel;


enum Update:int {
    case Files      = 0;
    case Themes     = 1;
    case Update     = 2;
    case Upgrade    = 3;
    case Install    = 4;
}

enum Results:int {
    case Done       = 0;
    case Warning    = 1;
    case Failed     = 2;
}

class MepPredict extends DecisionTree {

    private const PROGRAM_NAME      =    'Mep Predict';
    private const PROGRAM_AUTHOR    =    'David Lhoumaud';
    private const PROGRAM_VERSION   =    'v0.0.1';
    private const TEXT_SUCCESS      =    "\033[32m";
    private const TEXT_WARNING      =    "\033[33m";
    private const TEXT_ERROR        =    "\033[31m";
    private const TEXT_DEBUG        =    "\033[35m";
    private const TEXT_IMPORT       =    "\033[36m";
    private const TEXT_BOLD         =    "\033[1m";
    private const TEXT_NONE         =    "\033[0m";
    private $result;
    private $filename;
    
    public function __construct() {
        $this->result=array(
            'code'=>0,
            'error'=>null,
            'content'=>null
        );
        self::__params();
        $this->db=[[],[]];
        if (file_exists($this->filename)){
            $this->db=json_decode(file_get_contents($this->filename), true);
        }
        if ($this->run) self::run();
        if ($this->add) self::add();
    }

    /**
     * Gestion des paramètres
     */
    private function __params(){
        $shortopts          = 'h'          // help
                             .'r'          // run predict
                             .'a'          // save data
                             .'f:';        // filename
        $longopts           = [
                                'action:',
                                'date:',
                                'result:',
                            ];
        $options            = getopt($shortopts, $longopts);
        $this->run          = (isset($options['r'])?true:false);
        $this->add          = (isset($options['a'])?true:false);
        $this->action_db    = self::arg_to_update_action(strtolower($options['action']??''));
        $this->date_db      = (isset($options['date'])?strtotime($options['date']):time());
        $this->result_db    = self::arg_to_result_value(strtolower($options['result']??''));
        $this->filename     = (isset($options['f'])?$options['f']:'db.json');
        self::__help((isset($options['h'])?true:false), 0);
    }

    /**
     * Gestion d'affiche des message de log
     *
     * @param String $message texte à afficher
     * @param String $message_type Type de log à afficher (vert, rouge, jaune)
     * @param Boolean $return si true on return le message
     * @return String si le paramètre $return est true alors on retourne $message
     */
    private function Log($message, $message_type='standard', $return=false){
        $date=date("d-m-Y H:i:s");
        switch ($message_type) {
            case 'ok':
            case 'success':
                $code=($this->result['code']==0?'OK':$this->result['code']);
                $final_message    = self::TEXT_SUCCESS.self::TEXT_BOLD.$date."\t".$code.self::TEXT_NONE."\t".self::TEXT_SUCCESS.$message.self::TEXT_NONE;
            break;
            case 'warn':
            case 'warning':
                $code=($this->result['code']==0?'WARN':$this->result['code']);
                $final_message    = self::TEXT_WARNING.self::TEXT_BOLD.$date."\t".$code.self::TEXT_NONE."\t".self::TEXT_WARNING.$message.self::TEXT_NONE;
            break;
            case 'err':
            case 'error':
                $code=($this->result['code']==0?'ERR':$this->result['code']);
                $final_message    = self::TEXT_ERROR.self::TEXT_BOLD.$date."\t".$code.self::TEXT_NONE."\t".self::TEXT_ERROR.$message.self::TEXT_NONE;
            break;
            case 'dbg':
            case 'debug':
                $code=($this->result['code']==0?'DBG':$this->result['code']);
                $final_message    = self::TEXT_DEBUG.self::TEXT_BOLD.$date."\t".$code.self::TEXT_NONE."\t".self::TEXT_DEBUG.$message.self::TEXT_NONE;
            break;
            case 'info':
                $code=($this->result['code']==0?'INFO':$this->result['code']);
            case 'imp':
            case 'import':
                $code=$code??($this->result['code']==0?'IMP':$this->result['code']);
                $final_message    = self::TEXT_IMPORT.self::TEXT_BOLD.$date."\t".$code.self::TEXT_NONE."\t".self::TEXT_IMPORT.$message.self::TEXT_NONE;
            break;
            default:
                $final_message = $message;
            break;
        }
        if (!$return) echo $final_message."\n";
        else return $final_message."\n";
    }

    /**
     * Afficher l'aide
     *
     * @param Boolean $display affiche ou pas le echo
     * @param Integer $code code de sortie du exit()
     */
    private function __help($display=true, $code=0){
        if ($display){
            self::Log(self::PROGRAM_NAME.' '.self::PROGRAM_VERSION.'
by '.self::PROGRAM_AUTHOR.'
Options:
    -r    Lance une prédiction
        --action=(files|themes|update|upgrade|install)
        --date=\'Y-m-d\'
    -a    Ajoute une entrée dans la base de donnée json
        --action=(files|themes|update|upgrade|install)
        --date=\'Y-m-d\'
        --result=(done|warning|failed)
    -f    Chemin de la base de donnée json (default: db.json)
    -h    Afficher l\'aide
');
            exit($code);
        }
    }

    private function update_action($id) {
        switch ($id) {
            case Update::Files->value   : return 'Files';
            case Update::Themes->value  : return 'Themes';
            case Update::Update->value  : return 'Update';
            case Update::Upgrade->value : return 'Upgrade';
            case Update::Install->value : return 'Install';
            default                     : return 'Unknown';
        }
    }

    private function result_value($id) {
        switch ($id) {
            case Results::Done->value       : return 'Done';
            case Results::Warning->value    : return 'Warning';
            case Results::Failed->value     : return 'Failed';
            default                         : return 'Unknown';
        }
    }

    private function arg_to_update_action($id) {
        switch ($id) {
            case 'files'    : return Update::Files->value;
            case 'themes'   : return Update::Themes->value;
            case 'update'   : return Update::Update->value ;
            case 'upgrade'  : return Update::Upgrade->value;
            case 'install'  : return Update::Install->value;
            default         : return -1;
        }
    }

    private function arg_to_result_value($id) {
        switch ($id) {
            case 'done'     : return Results::Done->value;
            case 'warning'  : return Results::Warning->value;
            case 'failed'   : return Results::Failed->value;
            default         : return -1;
        }
    }

    private function run() {
        if (file_exists($this->filename)){
            $datas=$this->db[0];
            $results=$this->db[1];
        } else {
            $datas=[];
            $results=[];
            for($i=0; $i<1000; $i++){
                $date=mt_rand(1262055681,time());
                $result=mt_rand(0,2);
                $type=mt_rand(0,4);
                echo self::update_action($type)."\t-> ".date('Y-m-d l', $date)."\t: ".self::result_value($result)."\n";
                $datas[]=explode(',', $type.','.date('L,n,z,j,w', $date));
                $results[]=$result;
            }
            
            file_put_contents($this->filename, json_encode([$datas, $results], JSON_NUMERIC_CHECK));
        }

        // $classifier = new DecisionTree();
        // self::train($datas, $results);
        // echo "============= PREDICT =============\n";
        // for ($i=0; $i <= 4; $i++) {
        //     echo self::update_action($i)
        //         ."\t-> ".date('Y-m-d l', $this->date_db)."\t: "
        //         .self::result_value(
        //             self::predict(
        //                 explode(',', $i.','.date('L,n,z,j,w', $this->date_db))
        //             )
        //         )."\n";
        // }

        $regression = new SVR(Kernel::LINEAR);
        $regression->train($datas, $results);
        var_dump($regression->predict(explode(',', $i.','.date('L,n,z,j,w', $this->date_db))));
    }

    private function add() {
        self::Log(
            "Add entry in JSON DB\n"
            .self::update_action($this->action_db)
            ."\t-> ".date('Y-m-d l', $this->date_db)."\t: "
            .self::result_value($this->result_db)
        );
        $this->db[0][] = explode(',', $this->action_db.','.date('L,n,z,j,w', $this->date_db));
        $this->db[1][] = $this->result_db;
        file_put_contents($this->filename, json_encode([$this->db[0], $this->db[1]], JSON_NUMERIC_CHECK));
    }
}
return new MepPredict();
