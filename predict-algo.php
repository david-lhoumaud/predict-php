<?php

// Algorithme de prédiction
function predict($x, $y) {
    // Calculer la moyenne des valeurs x et y
    $mean_x = array_sum($x) / count($x);
    $mean_y = array_sum($y) / count($y);
    
    // Calculer les différences entre les valeurs x et y et leur moyenne
    $diff_x = array();
    $diff_y = array();
    foreach($x as $val) {
        $diff_x[] = $val - $mean_x;
    }
    foreach($y as $val) {
        $diff_y[] = $val - $mean_y;
    }
    
    // Calculer le produit des différences
    $prod_diff = array();
    for($i=0; $i<count($x); $i++) {
        $prod_diff[] = $diff_x[$i] * $diff_y[$i];
    }
    
    // Calculer le carré des différences x
    $sq_diff_x = array();
    foreach($diff_x as $val) {
        $sq_diff_x[] = pow($val, 2);
    }
    
    // Calculer la somme des produits des différences et le carré des différences x
    $sum_prod_diff = array_sum($prod_diff);
    $sum_sq_diff_x = array_sum($sq_diff_x);
    
    // Calculer la pente
    $m = $sum_prod_diff / $sum_sq_diff_x;
    
    // Calculer l'ordonnée à l'origine
    $b = $mean_y - ($m * $mean_x);
    
    // Retourner la pente et l'ordonnée à l'origine
    return array("m" => $m, "b" => $b);
}

// Exemple d'utilisation
$x = array(1, 2, 3, 4, 5);
$y = array(2, 4, 6, 8, 10);
$prediction = predict($x, $y);
echo "La pente est de " . $prediction['m'] . " et l'ordonnée à l'origine est de " . $prediction['b'];

?>